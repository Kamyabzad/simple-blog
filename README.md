<h1>Simple Blog</h1>
This project is implementation of a simple blog which is authored by several people who could freely register.

<h2>Built With</h2>
* <h5>Gargron Fileupload:</h5>
A library for OOP-based file uploading
* <h5>Phpmailer:</h5>
A library for OOP-based mail management
* <h5>Symfony Twig:</h5>
Templating engine for php
* <h5>Symfony Http-foundation:</h5>
Providing an OOP layer for handling http requests and handling post and get data to control interactions between client and the server.
* <h5>Symfony Forms:</h5>
Providing an OOP layer for handling user interaction with data to avoid being vulnerable to malicious form requests like CSRF attacks. This specific library provides a decent integration with Twig engine for templating forms.
* <h5>Symfony Routing</h5>
For producing more user-friendly URLS in the website.
* <h5>Dotenv:</h5>
A dependency module for loading variables from a .env file

<h2>Author</h2>
@Kamiyabta