<?php

use app\SessionManager;

require_once __DIR__.'/config.php';
require_once __DIR__.'/bootstrap.php';

$builder = new \DI\ContainerBuilder();
$builder->addDefinitions(__DIR__.'/./container_definitions.php');
$container = $builder->build();

$sessionManager = $container->get(SessionManager::class);
$session = $sessionManager->getSession();
$session->start();

$posts = $entityManager->getRepository(':Post')
    ->findAll();

$loader = new Twig_Loader_Filesystem(__DIR__.'/templates');
$twig = new Twig_Environment($loader);
$twig->addGlobal('session', $session);

echo $twig->render('blog.twig', array('posts' => $posts));