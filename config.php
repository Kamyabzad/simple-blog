<?php
use PHPMailer\PHPMailer\PHPMailer;

require 'vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

$hostname = getenv('DB_HOST');
$user = getenv('DB_USERNAME');
$password = getenv('DB_PASSWORD');
$dbname = getenv('DB_DATABASE');

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
$mail->SMTPDebug = 2;                                 // Enable verbose debug output
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = getenv('MAIL_HOST');
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = getenv('MAIL_USERNAME');	                 // SMTP username
$mail->Password = getenv('MAIL_PASSWORD');                           // SMTP password
$mail->SMTPSecure = getenv('MAIL_SMTP_SECURE');                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = getenv('MAIL_PORT');                                    // TCP port to connect to
$mail->SMTPDebug = false;
$mail->setFrom(getenv('MAIL_FROM_ADDRESS'), getenv('MAIL_FROM_NAME'));
$mail->addReplyTo(getenv('MAIL_REPLY_ADDRESS'), getenv('MAIL_REPLY_NAME'));
$mail->isHTML(true);
?>
