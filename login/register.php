<?php

use app\UserManager;
use app\ValidationManager;
use models\Author;
use app\SessionManager;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

require __DIR__.'/../config.php';
require __DIR__.'/../form_setup.php';
require __DIR__.'/../bootstrap.php';
require __DIR__.'/../container_definitions.php';

$builder = new \DI\ContainerBuilder();
$builder->addDefinitions(__DIR__.'/../container_definitions.php');
$container = $builder->build();

$sessionManager = $container->get(SessionManager::class);
$sessionManager->getSession()->start();

if ($sessionManager->isLoggedIn()) {
    echo "<script>window.open('/blog/panel/new-post','_self')</script>";
} else {
    $request = Request::createFromGlobals();

    $form = $formFactory->createBuilder()
        ->add('email', TextType::class)
        ->add('password', PasswordType::class)
        ->add('register', SubmitType::class)
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted()) {
        $formData = $form->getData();
        $email = $formData['email'];
        $password = $formData['password'];

        $validationManager = $container->get(ValidationManager::class);

        $errors = $validationManager->registerValidation($email, $password);

        if (empty($errors)) {
            $userManager = $container->get(UserManager::class);
            $userManager->createUser($email, $password);
            echo "<script>window.open('/blog/login/','_self')</script>";
        }

    }

    echo $twig->render('register.twig', array(
        'errors' => $errors,
        'form' => $form->createView(),
        'session' => $sessionManager->getSession()));
}

?>