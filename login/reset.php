<?php
use app\SessionManager;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;


require_once __DIR__.'/../config.php';
require_once __DIR__.'/../form_setup.php';
require_once __DIR__.'/../bootstrap.php';

$builder = new \DI\ContainerBuilder();
$builder->addDefinitions(__DIR__.'/../container_definitions.php');
$container = $builder->build();

$sessionManager = $container->get(SessionManager::class);
$sessionManager->getSession()->start();

if ($sessionManager->isLoggedIn()) {
    echo "<script>window.open('/blog/panel/new-post','_self')</script>";
} else {
    $request = Request::createFromGlobals();

    $form = $formFactory->createBuilder()
        ->add('password', PasswordType::class)
        ->add('selector', HiddenType::class, array(
            'data' => $request->query->get('selector')
        ))
        ->add('validator', HiddenType::class, array(
            'data' => $request->query->get('validator')
        ))
        ->add('reset', SubmitType::class)
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted()) {

        $formData = $form->getData();
        $selector = $formData['selector'];
        $validator = $formData['validator'];
        $password = $formData['password'];


        $validateToken = false;

        $time = time();

        $passwordReset = $entityManager->getRepository(':PasswordReset')
            ->findOneBy(array('selector' => $selector));

        if ($passwordReset == null) {
            $validateToken = false;
        } else {
            $author = $passwordReset->getAuthor();

            $calc = hex2bin($validator);

            if ($passwordReset->getExpires() >= $time && $calc == $passwordReset->getValidator()) {
                $validateToken = true;
            } else {
                $validateToken = false;
            }
        }

        if ($validateToken == true) {

            $author->setPassword(password_hash($password, PASSWORD_DEFAULT));
            $entityManager->persist($author);
            $entityManager->remove($passwordReset);
            $entityManager->flush();
            echo "<script>window.open('/blog/login','_self')</script>";
        } else {
            echo "لینک اشتباه است یا منقضی شده.";
        }

    }

    echo $twig->render('reset.twig', array(
        'form' => $form->createView(),
        'session' => $sessionManager->getSession()
    ));

}
?>