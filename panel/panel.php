<?php
use models\Post;
use app\SessionManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require __DIR__.'/../config.php';
require __DIR__.'/../form_setup.php';
require __DIR__.'/../bootstrap.php';

$builder = new \DI\ContainerBuilder();
$builder->addDefinitions(__DIR__.'/../container_definitions.php');
$container = $builder->build();

$sessionManager = $container->get(SessionManager::class);
$session = $sessionManager->getSession();
$session->start();

if (!$sessionManager->isLoggedIn()) {
    echo "<script>window.open('/blog/login','_self')</script>";
} else {
    $request = Request::createFromGlobals();

    $form = $formFactory->createBuilder()
        ->add('title', TextType::class)
        ->add('text', TextAreaType::class)
        ->add('post', SubmitType::class)
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted()) {
        $formData = $form->getData();
        $email = $session->get('email');
        $title = $formData['title'];
        $text = $formData['text'];

        $author = $entityManager->getRepository(':Author')
            ->findOneBy(array('email' => $email));

        $post = new Post();
        $post->setAuthor($author);
        $post->setTitle($title);
        $post->setText($text);

        $entityManager->persist($post);
        $entityManager->flush();

    }


    echo $twig->render('panel.twig', array(
        'form' => $form->createView(),
        'session' => $session
    ));

}
?>