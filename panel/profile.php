<?php
use models\Author;
use app\SessionManager;

require __DIR__.'/../config.php';
require __DIR__.'/../bootstrap.php';

$builder = new \DI\ContainerBuilder();
$builder->addDefinitions(__DIR__.'/../container_definitions.php');
$container = $builder->build();

$sessionManager = $container->get(SessionManager::class);
$session = $sessionManager->getSession();
$session->start();

if (!$sessionManager->isLoggedIn()) {
    echo "<script>window.open('/blog/login','_self')</script>";
} else {
    $email = $session->get('email');

    $author = $entityManager->getRepository(':Author')
        ->findOneBy(array('email' => $email));

// Specify our Twig templates location
    $loader = new Twig_Loader_Filesystem(__DIR__.'/../templates');

// Instantiate our Twig
    $twig = new Twig_Environment($loader);
    $twig->addGlobal('session', $session);


    echo $twig->render('profile.twig', array('author' => $author));
}
?>