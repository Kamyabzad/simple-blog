<?php
namespace app;

use Doctrine\ORM\EntityManager;
use models\Author;

class UserManager
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createUser ($email, $password)
    {
        $password = password_hash($password, PASSWORD_DEFAULT);

        $author = new Author();
        $author->setEmail($email);
        $author->setPassword($password);

        $this->entityManager->persist($author);
        $this->entityManager->flush();
    }
}