<?php
namespace app;

use Doctrine\ORM\EntityManager;

class ValidationManager
{
    private $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function registerValidation($email, $password)
    {
        $errors = [];

        if ($email == '' || $password == '')
            $errors['empty'] = true;

        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            $errors['invalid_email'] = true;

        if ($this->entityManager->getRepository(':Author')
            ->findOneBy(array('email' => $email)))
            $errors['already_existing'] = true;

        return $errors;
    }

    public function loginValidation($email, $password)
    {
        $errors = [];

        $author = $this->entityManager->getRepository(':Author')
            ->findOneBy(array('email' => $email));

        if ($author != null)
            $password_hashed = $author->getPassword();
        else
            $password_hashed = null;

        if ($password_hashed == null || !password_verify($password, $password_hashed))
            $errors['incorrect_combination'] = true;

        return $errors;
    }

}