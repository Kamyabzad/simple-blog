<?php
namespace models;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 @Entity @Table(name="posts")
 **/
class Post
{

    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(type="string") **/
    protected $title;

    /** @Column(type="text") **/
    protected $text;

    /**
     * @ManyToOne(targetEntity="Author", inversedBy="posts")
     * @JoinColumn(name="author_id", referencedColumnName="id")
     */
    protected $author;



    public function getId()
    {
        return $this->id;
    }


    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text): void
    {
        $this->text = $text;
    }

    /**
     * Set author.
     *
     * @param \models\Author|null $author
     *
     * @return Post
     */
    public function setAuthor(\models\Author $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author.
     *
     * @return \models\Author|null
     */
    public function getAuthor()
    {
        return $this->author;
    }
}
